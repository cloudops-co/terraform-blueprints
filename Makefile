DEFAULT_BRANCH := main

VERSION_FILE := VERSION
CURRENT_VERSION := $(shell cat $(VERSION_FILE))

NEW_VERSION_SCRIPT := ./scripts/get-new-project-version.sh
NEW_VERSION := $(shell $(NEW_VERSION_SCRIPT) $(CURRENT_VERSION))

.PHONY: help
help:  ## Show available commands
	@echo "Available commands:"
	@echo
	@sed -n -E -e 's|^([A-Za-z0-9/_-]+):.+## (.+)|\1@\2|p' $(MAKEFILE_LIST) | column -s '@' -t

.PHONY: lint
lint:  ## Run lint commands
	pre-commit run --all-files --verbose --show-diff-on-failure --color always

.PHONY: fmt
fmt:  ## Format all Terraform and Terragrunt files
	terraform fmt -recursive aws
	terragrunt hclfmt

.PHONY: bump-version
bump-version:  ## Bump the project version
	git checkout $(DEFAULT_BRANCH)
	@echo
	bump2version --new-version $(NEW_VERSION) major
	@echo
	@echo "Version bumped to '$(NEW_VERSION)'! Please check if the new tag is OK:"
	@echo
	git show v$(NEW_VERSION)
	@echo
	@echo "Looks good? If so, run 'make release' to push the new version to the repository."

.PHONY: release
release:  ## Push the new project version
	git push --follow-tags origin $(DEFAULT_BRANCH)

.PHONY: clean-cache
clean-cache:  ## Remove Terraform-related temporary folders
	find . -type d \( -name '.terragrunt-cache' -o -name '.terraform' \) -print -prune -exec rm -rf '{}' \;

.PHONY: blueprint
blueprint:  ## Create a new blueprint
	cookiecutter templates/blueprint --output-dir .
