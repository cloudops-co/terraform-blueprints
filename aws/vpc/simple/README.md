# Simple

:octicons-file-code-24: Status: [stable](/categories#stability) ·
:octicons-people-24: Target: [general](/categories#target-audience) ·
:octicons-mortar-board-24: Difficulty: [easy](/categories#difficulty)

This blueprint creates a (foo queue | a cluster of bar instances | etc).

<!-- TODO: remove this block if it's not applicable
## Best practices

It's recommended to enable X when Y is greater than Z because (...)
-->

<!-- TODO: remove this block if it's not applicable
## Known issues

- When X is enabled, Y can misbehave. This is a known AWS issue.
-->

<!-- TODO: remove this block if it's not applicable
## Limitations

This component currently does not support multi-account peerings.
-->

<!-- TODO: remove this block if it's not applicable
## Roadmap

### X split

X will be splitted to a standalone blueprint to address (...)

### Y refactoring

There's a current work on refactoring Y so the blueprint can be more (...)
-->

## Examples

!!! question "How do I deploy this?"
    See the [related Knowledge Base page](https://knowledge-base.ifoodcorp.com.br/engineering/cloud-infrastructure/iac/deploying/){: target="_blank" }
    for tutorials and full documentation about deploying components.

    Also, see the [Inputs](#inputs) section below for all available parameters.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 3.14.0 |

## Resources

| Name | Type |
|------|------|
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cidr"></a> [cidr](#input\_cidr) | CIDR block | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | VPC name | `string` | n/a | yes |
| <a name="input_allow_cidr_blocks"></a> [allow\_cidr\_blocks](#input\_allow\_cidr\_blocks) | A list of CIDR blocks to be allowed on the VPC default security group | `list(map(string))` | `[]` | no |
| <a name="input_enable_dns_hostnames"></a> [enable\_dns\_hostnames](#input\_enable\_dns\_hostnames) | Enable private DNS resolutions in the VPC. Required when using Route 53 private hosted zones in the account | `bool` | `false` | no |
| <a name="input_enable_vpn_gateway"></a> [enable\_vpn\_gateway](#input\_enable\_vpn\_gateway) | Whether to create a new VPN Gateway resource and attach it to the VPC | `bool` | `false` | no |
| <a name="input_one_nat_gateway_per_az"></a> [one\_nat\_gateway\_per\_az](#input\_one\_nat\_gateway\_per\_az) | Enable to provision only one NAT Gateway per availability zone | `bool` | `true` | no |
| <a name="input_propagate_private_route_tables_vgw"></a> [propagate\_private\_route\_tables\_vgw](#input\_propagate\_private\_route\_tables\_vgw) | Whether to propagate VPN gateway routes to the private route tables | `bool` | `true` | no |
| <a name="input_propagate_public_route_tables_vgw"></a> [propagate\_public\_route\_tables\_vgw](#input\_propagate\_public\_route\_tables\_vgw) | Whether to propagate VPN gateway routes to the public route tables | `bool` | `true` | no |
| <a name="input_single_nat_gateway"></a> [single\_nat\_gateway](#input\_single\_nat\_gateway) | Enable to provision a single shared NAT Gateway across all of the private networks | `bool` | `false` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_private_subnets"></a> [private\_subnets](#output\_private\_subnets) | List of IDs of private subnets |
| <a name="output_public_subnets"></a> [public\_subnets](#output\_public\_subnets) | List of IDs of public subnets |
| <a name="output_vpc_arn"></a> [vpc\_arn](#output\_vpc\_arn) | VPC ARN |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | VPC ID |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
