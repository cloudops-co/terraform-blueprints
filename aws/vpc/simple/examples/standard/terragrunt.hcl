terraform {
  source = "git::ssh://git@git.ifoodcorp.com.br/ifood/sre/cloud-infra/terraform/blueprints.git//aws/vpc/simple?ref=vCHANGEME"
}

include {
  path = find_in_parent_folders()
}

locals {
  service_vars = read_terragrunt_config(find_in_parent_folders("service.hcl")).locals
}

inputs = {
  name = "standard-example"

  tags = local.service_vars.tags
}
