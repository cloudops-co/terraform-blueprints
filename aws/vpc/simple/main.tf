# ------------------------------------------------------------------------------
# LOCALS
# ------------------------------------------------------------------------------

locals {

  public_subnet_cidrs = [
    cidrsubnet(var.cidr, 5, 0), # e.g. 10.0.0.0/21
    cidrsubnet(var.cidr, 5, 1), # e.g. 10.0.8.0/21
    cidrsubnet(var.cidr, 5, 2),
    cidrsubnet(var.cidr, 5, 3),
    cidrsubnet(var.cidr, 5, 4),
  ]

  private_subnet_cidrs = [
    cidrsubnet(var.cidr, 5, 5),
    cidrsubnet(var.cidr, 5, 6),
    cidrsubnet(var.cidr, 5, 7),
    cidrsubnet(var.cidr, 5, 8),
    cidrsubnet(var.cidr, 5, 9),
  ]

  number_of_azs_per_region = {
    us-east-1 = 5
    us-east-2 = 3
    sa-east-1 = 3
  }

  number_of_azs   = local.number_of_azs_per_region[data.aws_region.current.name]
  public_subnets  = slice(local.public_subnet_cidrs, 0, local.number_of_azs)
  private_subnets = slice(local.private_subnet_cidrs, 0, local.number_of_azs)

}



# ------------------------------------------------------------------------------
# DATA SOURCES
# ------------------------------------------------------------------------------

data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state = "available"

  # Some AZs have datacenter or instance type limitations and should not be used.
  # They can all be included in this same list, even for different regions.
  #
  # !!! WARNING !!!
  #
  # Changing this list may trigger the *DESTRUCTION* of subnets on VPCs already
  # provisioned in the affected regions!
  #
  # CHANGE ONLY IF YOU KNOW WHAT YOU ARE DOING AND AFTER *THOROUGHLY* TESTING!
  exclude_names = [
    "us-east-1e",
  ]
}

# ------------------------------------------------------------------------------
# VPC
# ------------------------------------------------------------------------------

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.0"

  name = var.name
  cidr = var.cidr

  azs = data.aws_availability_zones.available.names

  public_subnets  = local.public_subnets
  private_subnets = local.private_subnets

  private_subnet_tags = {
    SubnetTier                        = "Private"
    "kubernetes.io/role/internal-elb" = 1
  }

  public_subnet_tags = {
    SubnetTier               = "Public"
    "kubernetes.io/role/elb" = 1
  }

  # NAT Gateway
  # See https://github.com/terraform-aws-modules/terraform-aws-vpc#nat-gateway-scenarios
  enable_nat_gateway     = true
  one_nat_gateway_per_az = var.one_nat_gateway_per_az
  single_nat_gateway     = var.single_nat_gateway

  # DNS settings
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = true

  # VPN Gateway
  enable_vpn_gateway                 = var.enable_vpn_gateway
  propagate_private_route_tables_vgw = var.propagate_private_route_tables_vgw
  propagate_public_route_tables_vgw  = var.propagate_public_route_tables_vgw

  # Default security group
  manage_default_security_group = true
}
