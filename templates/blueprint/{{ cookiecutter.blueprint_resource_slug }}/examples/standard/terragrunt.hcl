terraform {
  source = "git::ssh://git@git.ifoodcorp.com.br/ifood/sre/cloud-infra/terraform/blueprints.git//{{ cookiecutter.base_dir }}/{{ cookiecutter.blueprint_service_slug }}/{{ cookiecutter.blueprint_resource_slug }}?ref=vCHANGEME"
}

include {
  path = find_in_parent_folders()
}

locals {
  service_vars = read_terragrunt_config(find_in_parent_folders("service.hcl")).locals
}

inputs = {
  name = "standard-example"

  tags = local.service_vars.tags
}
