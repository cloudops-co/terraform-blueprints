output "{{ cookiecutter.blueprint_resource_slug | replace('-', '_') }}_arn" {
  description = "The {{ cookiecutter.blueprint_pretty_name }} ARN"
  value       = module.{{ cookiecutter.blueprint_module_name }}.arn
}
