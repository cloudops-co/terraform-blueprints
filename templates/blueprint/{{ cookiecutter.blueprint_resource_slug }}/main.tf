# ------------------------------------------------------------------------------
# {{ cookiecutter.blueprint_pretty_name | upper }}
# ------------------------------------------------------------------------------

module "{{ cookiecutter.blueprint_module_name }}" {
  # source  = "git::ssh://git@git.ifoodcorp.com.br/ifood/sre/cloud-infra/terraform/modules/aws/terraform-aws-foo.git?ref=v1.0.0"
  # source  = "terraform-aws-modules/foo/aws"
  # version = "1.0.0"

  name = var.name

  tags = module.tags.tags
}
