# ------------------------------------------------------------------------------
# ENVIRONMENT
# ------------------------------------------------------------------------------

variable "environment" {
  description = "The environment name"
  type        = string
}

variable "tags" {
  description = "Key-value mapping of resource tags"
  type        = map(string)
}

# ------------------------------------------------------------------------------
# {{ cookiecutter.blueprint_pretty_name | upper }}
# ------------------------------------------------------------------------------

variable "name" {
  description = "The name of the {{ cookiecutter.blueprint_pretty_name }}"
  type        = string
}
