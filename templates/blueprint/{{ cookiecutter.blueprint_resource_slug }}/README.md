# {{ cookiecutter.blueprint_pretty_name }}

:octicons-file-code-24: Status: [stable](/categories#stability) ·
:octicons-people-24: Target: [general](/categories#target-audience) ·
:octicons-mortar-board-24: Difficulty: [easy](/categories#difficulty)

This blueprint creates a (foo queue | a cluster of bar instances | etc).

<!-- TODO: remove this block if it's not applicable
## Best practices

It's recommended to enable X when Y is greater than Z because (...)
-->

<!-- TODO: remove this block if it's not applicable
## Known issues

- When X is enabled, Y can misbehave. This is a known AWS issue.
-->

<!-- TODO: remove this block if it's not applicable
## Limitations

This component currently does not support multi-account peerings.
-->

<!-- TODO: remove this block if it's not applicable
## Roadmap

### X split

X will be splitted to a standalone blueprint to address (...)

### Y refactoring

There's a current work on refactoring Y so the blueprint can be more (...)
-->

## Examples

!!! question "How do I deploy this?"
    See the [related Knowledge Base page](https://knowledge-base.ifoodcorp.com.br/engineering/cloud-infrastructure/iac/deploying/){: target="_blank" }
    for tutorials and full documentation about deploying components.

    Also, see the [Inputs](#inputs) section below for all available parameters.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
