#!/usr/bin/env bash

set -e
set -o pipefail

# Environment variables
: "${DEBUG:=""}"

# Directories
BaseDir="{{ cookiecutter.base_dir }}"
BlueprintServiceDir="${BaseDir}/{{ cookiecutter.blueprint_service_slug }}"
BlueprintResourceDir="${BlueprintServiceDir}/{{ cookiecutter.blueprint_resource_slug }}"

# Resource names
BlueprintServiceSlug="{{ cookiecutter.blueprint_service_slug }}"
BlueprintResourceSlug="{{ cookiecutter.blueprint_resource_slug }}"
BlueprintModuleName="{{ cookiecutter.blueprint_module_name }}"

# See https://regex101.com/r/nRYRxd/1
ValidSlugRegex='^[a-z][a-z0-9-]*[a-z0-9]$'

# See https://regex101.com/r/Y04r7g/1
ValidModuleNameRegex='^[a-z][a-z0-9_]*[a-z0-9](_[a-z][a-z0-9_]*[a-z0-9])*$'


msg()
{
    echo -e "==> $*" >&2
}

main()
{
    # Run from repository root
    cd ..

    if [[ ! -f VERSION ]] ; then
        msg "Fatal: you must create the template in the repository root directory (current: $PWD)"
        exit 1
    fi

    if [[ -d "$BlueprintResourceDir" ]] ; then
        msg "Error: directory '$BlueprintResourceDir' already exists; aborting"
        exit 1
    fi

    if [[ -n "$DEBUG" ]] ; then
        msg "BaseDir = $BaseDir"
        msg "BlueprintServiceDir = $BlueprintServiceDir"
        msg "BlueprintResourceDir = $BlueprintResourceDir"
    fi

    local slug
    local exit_code=0

    for slug in \
        "$BlueprintServiceSlug" \
        "$BlueprintResourceSlug"
    do
        if [[ ! "$slug" =~ $ValidSlugRegex ]] ; then
            msg "Error: invalid slug '$slug'; please use only lowercase letters, numbers and dashes (-)"
            exit_code=1
        fi
    done

    if [[ ! "$BlueprintModuleName" =~ $ValidModuleNameRegex ]] ; then
        msg "Error: invalid module name '$BlueprintModuleName'; please use only lowercase letters, numbers and underscores (_)"
        exit_code=1
    fi

    exit $exit_code
}


main "$@"
