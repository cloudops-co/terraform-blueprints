#!/usr/bin/env bash

set -e
set -o pipefail

# Directories
BaseDir="{{ cookiecutter.base_dir }}"
BlueprintServiceDir="${BaseDir}/{{ cookiecutter.blueprint_service_slug }}"
BlueprintResourceDir="${BlueprintServiceDir}/{{ cookiecutter.blueprint_resource_slug }}"

# NOTE: this variable must match the template directory structure
GeneratedDir="{{ cookiecutter.blueprint_resource_slug }}"


msg()
{
    echo -e "==> $*" >&2
}

main()
{
    # Run from repository root
    cd ..

    if [[ ! -f VERSION ]] ; then
        msg "Fatal: you must create the template in the repository root directory (current: $PWD)"
        exit 1
    fi

    msg "Preparing blueprint directory"

    if ! mkdir -p "$BlueprintServiceDir" ; then
        msg "Error: could not create '$BlueprintServiceDir'; aborting"
        exit 1
    fi

    msg "Moving generated directory"

    if ! mv "$GeneratedDir" "$BlueprintResourceDir" ; then
        msg "Error: could not move generated directory to '$BlueprintResourceDir'; aborting"
        exit 1
    fi

    msg "Blueprint succesfully created on '$BlueprintResourceDir'. Enjoy!"
}


main "$@"
